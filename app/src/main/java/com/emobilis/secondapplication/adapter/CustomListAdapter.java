package com.emobilis.secondapplication.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.emobilis.secondapplication.R;
import com.emobilis.secondapplication.model.CustomListModel;

import java.util.List;

//we need to extend the ArrayAdapter class as we are building an adapter
public class CustomListAdapter extends ArrayAdapter<CustomListModel> {
    //the list values in the List of custom list model
    List<CustomListModel> customListModelList;

    //activity context
    Context context;

    //the layout resource file for the list items
    int resource;

    //constructor
    public CustomListAdapter(Context context, int resource, List<CustomListModel> customListModelList){
        super(context,resource,customListModelList);
        this.context = context;
        this.resource = resource;
        this.customListModelList = customListModelList;
    }

    @NonNull
    @Override
    //reference our layout and the view elements inside it
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //we need to get the view of the xml for our list item
        //And for this we need a layoutinflater
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        //getting the view
        View view = layoutInflater.inflate(resource, null, false);
        //create the ref for the views inside the layout
        ImageView imageView = view.findViewById(R.id.imageView);
        TextView textName = view.findViewById(R.id.textViewName);
        TextView textDesc = view.findViewById(R.id.textViewDescription);
        Button btnDelete = view.findViewById(R.id.btnDelete);

        //getting the item of the specified position
        CustomListModel customListModel = customListModelList.get(position);

        //getting data from our model
        imageView.setImageDrawable(context.getResources().getDrawable(customListModel.getImage()));
        textName.setText(customListModel.getTextName());
        textDesc.setText(customListModel.getTextDesc());
        //set the click listener
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem(position);
            }
        });

        //return
        return view;

    }

    private void removeItem(final int position) {
        //Creating an alert dialog to confirm the deletion
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this?");

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                     //remove item
                    //reference list
                customListModelList.remove(position);

                //reset list
                notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "Okay item won't be deleted", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNeutralButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        //intialize alert builder for it show
        //creating and displaying the alert dialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }




}
