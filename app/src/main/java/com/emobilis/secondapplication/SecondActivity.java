package com.emobilis.secondapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    //declare the element
    TextView sharedText;
    //
    String sharedTexts;
    //tag
    private static final String TAG = "SecondActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //create the ref
        sharedText = findViewById(R.id.sharedText);
        //get shared data
        Intent intent = getIntent();
        sharedTexts = intent.getStringExtra("text");
        Log.d(TAG,"shared text is " + sharedTexts);

        sharedText.setText(sharedTexts);


    }
}
