package com.emobilis.secondapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_nav);
        //intialize your components
        BottomNavigationView bottomNav = findViewById(R.id.bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        bottomNav.setItemIconTintList(null);


        //I added this if statement to keep the selected fragment when rotating the device
        if (savedInstanceState == null) {
            //how we begin a transaction for the fragments
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new FragmentA()).commit(); //how u load a fragment to an activity
        }
    }

    //here we call the BottomNavigationView Interface
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                    Fragment selectedfragment = null;

                    switch (item.getItemId()) {
                        case R.id.nav_fragmentA:
                           Intent intent = new Intent(BottomNavActivity.this,CustomListViewActivity.class);
                           startActivity(intent);
                            break;
                        case R.id.nav_fragmentB:
                            Intent intentB = new Intent(BottomNavActivity.this,ListViewActivity.class);
                            startActivity(intentB);
                            break;
                    }

//                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
//                            selectedfragment).commit();
                    return true;
                }
            };

}
