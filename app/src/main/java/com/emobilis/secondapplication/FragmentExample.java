package com.emobilis.secondapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class FragmentExample extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_example);
        //using the getSupportFragmentManger() to begin the transactions
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container_a, new FragmentFirst())
                .add(R.id.container_b, new FragmentSecond())
                .commit();
    }
}
