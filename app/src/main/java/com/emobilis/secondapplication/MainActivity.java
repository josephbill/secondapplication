package com.emobilis.secondapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //declare my tag filter for logs
    private static final String TAG = "MainActivity";
    //declare my element
    Button btnSubmit;
    TextView txClick;
    //variable text
    String sharedText = "Share Text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG,"onCreate is running");

        //forget to call the reference

        txClick = findViewById(R.id.textView);
        btnSubmit = findViewById(R.id.btnClick);

        intialise();


    }

    private void intialise() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,BottomNavActivity.class);
                startActivity(intent);
            }
        });

        txClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,NavigationSliderActivity.class);
                startActivity(intent);
            }
        });
    }

    public void fragments(View v){
        Intent intent = new Intent(MainActivity.this, FragmentExample.class);
        startActivity(intent);
    }

    //when interface is in focus
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG,"onstart is running");

    }
}
