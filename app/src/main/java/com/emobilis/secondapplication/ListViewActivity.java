package com.emobilis.secondapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ListViewActivity extends AppCompatActivity {
    //array of animals
    String[] animals = {"Cow","Donkey","Lion","Dog","Sheep","Wolf"};
    //declare element
    ListView listAnimals;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        //intialize my array adapter
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.list_item,animals);
        //create a reference to the ListView
        listAnimals  = findViewById(R.id.listExample);

        //attach my adapter to my list widget
        listAnimals.setAdapter(adapter);

        //inorder to set an onclick
        listAnimals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                 if (position == 0){
                     Toast.makeText(ListViewActivity.this, "Cow is clicked", Toast.LENGTH_SHORT).show();
                 } else if (position == 1){
                     Toast.makeText(ListViewActivity.this, "donkey is clicked", Toast.LENGTH_SHORT).show();

                 } else if (position == 2){
                     Toast.makeText(ListViewActivity.this, "lion is clicked", Toast.LENGTH_SHORT).show();

                 } else if(position == 3){
                     Toast.makeText(ListViewActivity.this, "dog is clicked", Toast.LENGTH_SHORT).show();

                 } else if(position == 4){
                     Toast.makeText(ListViewActivity.this, "sheep is clicked", Toast.LENGTH_SHORT).show();

                 } else if(position == 5){

                     Toast.makeText(ListViewActivity.this, "Wolf is clicked", Toast.LENGTH_SHORT).show();
                     Intent intent = new Intent(ListViewActivity.this,CustomListViewActivity.class);
                     startActivity(intent);
                 }
            }
        });

    }
}
