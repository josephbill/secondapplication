package com.emobilis.secondapplication.model;

public class CustomListModel {
    //creatle the variables for our views
    private String textName;
    private String textDesc;
    private int image;

    //constructor to intialize the attributes for the variables
    public CustomListModel(String textName,String textDesc,int image){
          this.textName = textName;
          this.textDesc = textDesc;
          this.image = image;
    }

    //create the getters and setters


    public String getTextName() {
        return textName;
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public String getTextDesc() {
        return textDesc;
    }

    public void setTextDesc(String textDesc) {
        this.textDesc = textDesc;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
