package com.emobilis.secondapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.emobilis.secondapplication.adapter.CustomListAdapter;
import com.emobilis.secondapplication.model.CustomListModel;

import java.util.ArrayList;
import java.util.List;

public class CustomListViewActivity extends AppCompatActivity {
    //a List for holding list items
    List<CustomListModel> customListModelList;
    //the listview
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);

        customListModelList = new ArrayList<>();
        //create ref to listview
        listView = findViewById(R.id.listView);

        //adding
        customListModelList.add(new CustomListModel("Smartphone","Nokia A5",R.drawable.first));
        customListModelList.add(new CustomListModel("Bicycle","BMX 1234",R.drawable.second));
        customListModelList.add(new CustomListModel("Car","New JEEP",R.drawable.third));
        customListModelList.add(new CustomListModel("Spanner","Nokia A5",R.drawable.four));

        //creating the adapter
        CustomListAdapter adapter = new CustomListAdapter(this, R.layout.customlist_item, customListModelList);

        //attaching adapter to the listview
        listView.setAdapter(adapter);

    }
}
