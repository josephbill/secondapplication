package com.emobilis.secondapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;


public class NavigationSliderActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener {
   //declare the layout
    private DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_slider);

        //find the view
        drawerLayout = findViewById(R.id.drawer_Layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        navigation
        NavigationView navigationView = findViewById(R.id.navigation_Item);
        navigationView.setNavigationItemSelectedListener(this);

        //intialize the toggle
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //transactions for the fragments
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new OneFragment()).commit();  // commint a fragment to the blank FrameLayout
            navigationView.setCheckedItem(R.id.nav_one); //set the link for item clicked for the fragment
        }
    }
    //here when a user is done with the navigation item
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.nav_custom:
                  //switch to activities
                Intent intentCustom = new Intent(NavigationSliderActivity.this,CustomListViewActivity.class);
                startActivity(intentCustom);
                break;
            case R.id.nav_list:
                 // switch to activities
                 Intent intentList = new Intent(NavigationSliderActivity.this,ListViewActivity.class);
                 startActivity(intentList);
                break;
            case R.id.nav_one:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new OneFragment()).commit();
                break;
            case R.id.nav_two:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new TwoFragment()).commit();
                break;
            case R.id.nav_send:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new ThirdFragment()).commit();
                break;
            case R.id.nav_share:
                Toast.makeText(this, "share the code", Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, "select an item", Toast.LENGTH_SHORT).show();


        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
