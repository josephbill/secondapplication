package com.emobilis.secondapplication;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private MutableLiveData<CharSequence> text = new MutableLiveData<>();

    //capturing the input from the user
    //holding that data for transaction
    public void setText(CharSequence input){
        text.setValue(input);
    }

    //method to use when getting the text from the first fragment
    LiveData <CharSequence> getText(){
        return text;
    }


}
